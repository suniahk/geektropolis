module.exports = function( grunt ) {
  grunt.initConfig( {
    pkg : grunt.file.readJSON( 'package.json' ),
    concurrent : {
      dev : {
        tasks : [
          'nodemon',
          'watch'
        ],
        options : {
          logConcurrentOutput : true
        }
      }
    },

    jshint : {
      options : {
        reporter : require('jshint-stylish') // use jshint-stylish to make our errors look and read good
      },

      // when this task is run, lint the Gruntfile and all js files in src
      build : [ 'Gruntfile.js', 'project/js/*.js' ]
    },

    uglify : {
      options : {
        banner : '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n',
        mangle : false
      },
      build : {
        files : {
          'dist/js/javascript-<%= pkg.version %>.min.js': [ 'node_modules/angular/angular.js', 'node_modules/angular-route/angular-route.js', 'project/js/*.js' ]
        }
      }
    },

    sass : {
      build : {
        files : {
          'project/css/skeleton.css' : 'node_modules/skeleton-scss/scss/skeleton.scss',
          'project/css/base.css' : 'project/sass/base.scss',
          'project/css/layout.css' : 'project/sass/layout.scss',
          'project/css/module.css' : 'project/sass/module.scss',
          'project/css/state.css' : 'project/sass/state.scss',
          'project/css/theme.css' : 'project/sass/theme.scss'
        }
      }
    },

    cssmin : {
      options : {
        keepSpecialComments : 0
      },
      build : {
        files : {
          'dist/css/stylesheet-<%= pkg.version %>.min.css' : 'project/css/*.css'
        }
      }
    },

    watch : {
      files: ['project/**/*.css', 'project/**/*.sass'],
      tasks: ['sass', 'cssmin'],

      // for scripts, run jshint and uglify
      scripts: {
        files: 'project/**/*.js', tasks: ['jshint', 'uglify']
      }
    },

    nodemon : {
      dev : {
        script : 'project/core/server.js',
        watch : 'project/core/*.js'
      }
    },

    'string-replace' : {
      version : {
        files : {
          "dist/index.htm" : "project/index.htm"
        },
        options : {
          replacements: [{
            pattern: /{{ VERSION }}/g,
            replacement: '<%= pkg.version %>'
          }]
        }
      }
    },

    copy : {
      main : {
        files: [
          {
            expand : true,
            src : 'project/content/*',
            dest : 'dist/content',
            flatten : true
          },
          {
            expand : true,
            src : 'project/img/*',
            dest : 'dist/img',
            flatten : true
          }
        ]
      }
    }
  } );

  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks( 'grunt-string-replace' );

  grunt.registerTask( 'default', [ 'jshint', 'uglify', 'sass', 'cssmin', 'string-replace', 'copy' ] );
  grunt.registerTask( 'runServer', [ 'jshint', 'uglify', 'sass', 'cssmin', 'string-replace', 'copy', 'nodemon' ] );
  grunt.registerTask( 'continual', [ 'jshint', 'uglify', 'sass', 'cssmin', 'string-replace', 'copy', 'concurrent' ] );
};
