# Basic Information
## What You Will Need
 * Node.js (https://nodejs.org/en/)
 * MongoDB (https://www.mongodb.org/downloads#production)
 * Ruby and SASS (http://sass-lang.com/install)

## What This App Uses
 * MEAN Stack (MongoDB, Express, Angular, Node)
 * Mongoose ORM
 * SASS for CSS preprocessing

## After You Update Your Project
These steps are only necessary if you are pulling for the first time, or a change has been made to the project dependencies.
 * Open cli of choice, go to your project directory.
 * Run "npm update --save" in console.

## To Compile Everything
 * Run the command 'grunt' in your cli.
 * Run the command 'grunt continual' if you want to take advantage of watch.

## Commit Strategy
 * _DO NOT COMMIT TO MASTER!_  All major features should have their own branches.
 * When each feature is completed, it will be tested and merged into the main development branch.
 * Small patches can be done directly to the main branch.
 * Master should only be updated when live is updated.
