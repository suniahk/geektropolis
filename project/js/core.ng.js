var geektropolis = angular.module( "geektropolis", [ 'ngRoute' ] );

geektropolis.config( [ '$routeProvider', '$locationProvider', function( $routeProvider, $locationProvider ) {
  $locationProvider.html5Mode({ enabled: true, requireBase: false });
  $routeProvider.when( '/', {
    templateUrl: '/content/index.htm',
    controller: 'commonController',
  } ).when( '/menus', {
    templateUrl: '/content/menus.htm',
    controller: 'commonController',
  } ).when( '/contact', {
    templateUrl: '/content/contact.htm',
    controller: 'commonController',
  } ).when( '/login', {
    templateUrl: '/content/login.htm',
    controller: 'commonController',
  } ).when( '/community', {
    templateUrl: '/content/community.htm',
    controller: 'commonController',
  } ).when( '/404', {
    templateUrl: '/content/404.htm',
    controller: 'commonController',
  } ).otherwise( {
    redirectTo : '/404'
  } );
} ] );

geektropolis.controller( "commonController", ['$http', '$timeout', '$scope', '$location', function( $http, $timeout, $scope, $location ) {
  $scope.currentBackground = "";
  $scope.isActive = function( viewLocation ) {
    var isActivePage = viewLocation === $location.path();
    if( isActivePage ) {
      if( viewLocation != "/" ) {
        var viewString = viewLocation.replace( "/", "" );
        $scope.currentBackground = $scope.siteMap[ viewString ].backgroundImage;
      } else {
        $scope.currentBackground = $scope.siteMap.home.backgroundImage;
      }
    }
    return isActivePage;
  };

  $scope.siteMap = {
    "home" : { label: "Home", url : "/", backgroundImage : "img/concrete_seamless.png" },
    //"menus" : { label: "Menus", url : "/menus", backgroundImage : "" },
    "contact" : { label: "Contact", url : "/contact", backgroundImage : "img/concrete_seamless.png" },
    //"community" : { label: "Community", url : "/community", backgroundImage : "" },
    //"login" : { label: "Citizen Login", url : "/login", backgroundImage : "" }
    "kickstarter" : { label : "Kickstarter", url : "http://www.kickstarter.com" }
  };
} ] );
