var http = require( 'http' );
var express = require('express');
var passport = require( 'passport' );
var LocalStrategy = require( 'passport-local' );
var databaseConfig = require( './databaseLocation.js' );
var mongoose = require( 'mongoose' );
var session = require( 'express-session' );
var pbkdf2 = require( 'node-pbkdf2' );
var app = express();
var crypto = new pbkdf2( { iterations : 10000, saltLength : 12, derivedKeyLength : 30 } );
var packageInfo = require( '../../package.json' );

const PORT=8081;

// Database Abstraction
//app.use( session( { secret : "SecretGeektropolisAccess" } ) );
//app.use( passport.initialize() );
//app.use( passport.session() );
//app.set('view engine', 'express');


// Basic api triggers
app.use( express.static( __dirname.substring( 0, __dirname.indexOf( "\\project\\core" ) ) + "/dist" ) );

app.get('/app/*', function (req, res) {
    res.sendFile( __dirname.substring( 0, __dirname.indexOf( "\\project\\core" ) ) + "/dist/app.htm" );
});

app.get('*', function (req, res) {
    res.sendFile( __dirname.substring( 0, __dirname.indexOf( "\\project\\core" ) ) + "/dist/index.htm" );
});
//app.set('views', './');

//app.get( '/', function( request, response ) {
//  response.send( "Let's try this." );
//} );

var server = http.createServer( app );
server.listen( PORT );
console.log("Server listening on: http://localhost:%s", PORT);

if (process.platform === "win32") {
  require("readline")
    .createInterface({
      input: process.stdin,
      output: process.stdout
    })
    .on("SIGINT", function () {
      process.emit("SIGINT");
    });
}

process.on( 'SIGINT', function() {
  console.log("Server shutting down...");
  process.exit();
} );
